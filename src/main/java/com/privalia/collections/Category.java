package com.privalia.collections;

import java.util.Map;

public class Category {

	//crear una coleccion de tipo map que tenga pares clave valor long y list.
	private int idCategory;
	private String name;
	private Map<Long, Product> productList;
	/**
	 * @return the idCategory
	 */
	public int getIdCategory() {
		return idCategory;
	}
	/**
	 * @return the productList
	 */
	public Map<Long, Product> getProductList() {
		return productList;
	}
	/**
	 * @param productList the productList to set
	 */
	public void setProductList(Map<Long, Product> productList) {
		this.productList = productList;
	}
	/**
	 * @param idCategory the idCategory to set
	 */
	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
