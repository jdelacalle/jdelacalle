package com.privalia.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.privalia.collections.Student;

public class Main {

	public static void main(String[] args) {
		
	    List<Teacher> teacherList = new ArrayList<Teacher>();
	    
	    Teacher teacher1 = new Teacher();
	    teacher1.setIdTeacher(1);
	    teacher1.setName("Pepe");
	    teacherList.add(teacher1);
	    
	    Teacher teacher2 = new Teacher();
	    teacher2.setIdTeacher(2);
	    teacher2.setName("Alberto");
	    teacherList.add(teacher2);
	    
	    
		Student student1 = new Student();
		student1.setIdStudent(1);
		student1.setName("pepito");
		student1.setSurname("soto");
		student1.setAge(40);
		student1.setTeacher(teacherList);
		
		student1.getTeacher().forEach(item->System.out.println(item));
		
		
		Product product2 = new Product();
		product2.setIdProduct(1);
		product2.setName("tejanos");
		
		Map<Long, Product> productList = new HashMap<Long, Product>();
		productList.put(1L, product2);
		
		Category category = new Category();
		category.setIdCategory(1);
		category.setName("ropa");
		category.setProductList(productList);
		
		
		
		//mostrar el nombre de todos los productos partiendo de la category
		
		
		
		List studentList = new ArrayList();
		
		int counter = 4;
		
		//Ejemplo Boxing
		studentList.add(student1);
		//Ejemplo de Autoboxing
		studentList.add(counter);
		
		
		//Ejemplo de Unboxing
		Student newStudent = (Student)studentList.get(0);
		String name  = ((Student)studentList.get(0)).getName();
		
		//Lista generica asi no hace falta el boxing and unboxing
		List<Student> genericStudentList = new ArrayList<Student>();
		genericStudentList.add(student1);
		
		
		Student newStudent2 = genericStudentList.get(0);
		
	
		//NUEVO EN JAVA8 busquedas dentro de una coleccion
		//usa paralel programing que te lanza un thread para hacer multithread
		Optional<Student> optionalStudent = genericStudentList.stream().filter(p-> p.getName().equals("Pepe")).findFirst();
		Student searchStudent = optionalStudent.get();

	}

}
