package com.privalia.dao;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.privalia.model.Student;
import com.privalia.presentation.Main;
import com.privalia.util.FileUtil;
import com.privalia.util.MethodInfo;

public class StudentDao extends BaseDao implements IDao<Student>, INio<Student>{
	static final Logger logger = Logger.getLogger(Main.class);
	static Properties prop = null;
	static FileInputStream input = null;
	private Connection connect;
	static {
		try{
			input = new FileInputStream("src/main/resources/config.properties");
			prop.load(input);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new ExceptionInInitializerError(e);//wrapping de exceptions lanzando una runtime excepcion ya que el inicalizador estatico no me deja lanzar checked exceptions
		}
	}
	// Clases a utilizar bufferedWritter java.io
	// Otra clase es fileWritter de java.io
	//metodo equals y hascode, utilizaremos tb el __toString.
	// Agregar fichero alumno en un fichero de texto separado por comas
	//Frameworks que nos ayudan a picar codigo.
	//Creacion del fichero fuera
	//Integration test --> comprueba si el alumno existe. assertEqual.
	//tojson y toXml si hay tiempo
	@MethodInfo(author="Jorge de la calle",
			revision=2,
			comments="Writes line to a file",
			date = "03/09/2017")
	public int add(Student student) {
		
		String filename = prop.getProperty("filename");
		//si no haces try catch with resources ttienes que usar el finally para cerrar el buffer
		//en el ejemplo de profe usa el BufferedWriter
		
		try {
			FileUtil.create("", "Students.txt");
			FileUtil.write(student, FileUtil.getFile());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(Arrays.toString(e.getStackTrace()));
		}
		
		return student.getIdStudent();
	}

	@Override
	public int addWithNio(Student student) throws IOException {
		Path path = Paths.get(prop.getProperty("filename"));
		try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.forName("UTF-8"), StandardOpenOption.APPEND)) {
			writer.write(student.toString());
			writer.write(System.lineSeparator());
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw e;
		}
		return student.getIdStudent();
	}
	
	public int addStudentWithMySql(Student student) throws SQLException {
		
		int idStudent = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			connect = super.getConnection();
			pstmt = connect.prepareStatement("INSERT INTO student(name, surname, age" + "VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, student.getName());
			pstmt.setString(2, student.getSurname());
			pstmt.setInt(3, student.getAge());
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			rs.next();
			idStudent = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			pstmt.close();
			rs.close();
			connect.close();
		}
		
		return idStudent;
		
	}
}
