package com.privalia.model;

import java.util.UUID;

import com.privalia.util.IWritable;

public class Student extends PrivaliaObject implements IWritable {

	private int idStudent;
	private String name;
	private String surname;
	private int age;
	static int numero;
	
	//inicializador estatico que inicializa los atributos estaticos de la clase
	static{
		numero = 10;
	}
	
	/**
	 * @return the idStudent
	 */
	public int getIdStudent() {
		return idStudent;
	}
	/**
	 * @param idStudent the idStudent to set
	 */
	public void setIdStudent(int idStudent) {
		this.idStudent = idStudent;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	public Student()
	{
		super();
	}
	public Student(int idStudent, String name, 
			String surname, int age, UUID id)
	{
		super(id);
		this.idStudent = idStudent;
		this.name = name;
		this.surname = surname;
		this.age = age;
	}
	
	public Student(int idStudent, String name, 
			String surname, int age)
	{
		this.idStudent = idStudent;
		this.name = name;
		this.surname = surname;
		this.age = age;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + idStudent;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * cuando son comparaciones de atributos simples se hace con el == con el !=
	 * cuando son comparaciones de objects utilizamos equals.
	 * equals utilza el metodo hasCode que hace un calculo con numeros primos para comparar si los dos
	 * objetos son iguales.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (age != other.age)
			return false;
		if (idStudent != other.idStudent)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname)){ 
			return false;
		} else if (!uuid.equals(other.uuid)){
			return false;
		}
		return true;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Student [idStudent=");
		builder.append(idStudent);
		builder.append(", name=");
		builder.append(name);
		builder.append(", surname=");
		builder.append(surname);
		builder.append(", age=");
		builder.append(age);
		builder.append(", uuid=");
		builder.append(uuid);
		
		builder.append("]");
		
		return builder.toString();
	}

	public static Student getStudent()
	{
		return new Student();
	}
	
}
