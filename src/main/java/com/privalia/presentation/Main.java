package com.privalia.presentation;

import com.privalia.collections.Teacher;
import com.privalia.dao.StudentDao;
import com.privalia.model.Student;
import com.privalia.util.MethodInfo;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	static final Logger logger = Logger.getLogger(Main.class);
	
@MethodInfo(author="Francisco",
			revision=2,
			comments="My First commit",
			date = "22/09/2017")

	public static void main(String[] args) {	    
	    
		Student student1 = new Student();
		student1.setIdStudent(1);
		student1.setName("pepito");
		student1.setSurname("soto");
		student1.setAge(40);
		
		Student student2 = new Student(1, "Alberto", "Sierra", 44);
		
		System.out.println("student1 name " + student1.getName() + " student surname" + student1.getSurname());
		System.out.println("student1 name " + student2.getName() + " student surname" + student2.getSurname());
		
		logger.info("student1 name " + student1.getName() + " student surname" + student1.getSurname());
		logger.info("student1 name " + student2.getName() + " student surname" + student2.getSurname());

		Scanner reader = new Scanner(System.in);
		int option;
		do {	
			System.out.println("Escoge una opcion menu" );
			System.out.println("1 Añadir un alumno" );
			System.out.println("2 salir" );
			option = reader.nextInt();
	        switch (option)
	        {
        		case 1:
        			System.out.println("Nombre" );
					String name = reader.next();
					System.out.println("Apellido" );
					String apellido = reader.next();
					System.out.println("edad" );
					int edad = reader.nextInt();
					System.out.println("Id" );
					int id = reader.nextInt();
					logger.info("Nombre estudiante " + name);
					logger.info("apellido estudiante " + apellido);
					logger.info("edad estudiante " + edad);
					logger.info("id estudiante " + id);
					
					Student st = new Student(id, name, apellido, edad);
					StudentDao std = new StudentDao();
					std.add(st);
					
					break;
    			case 2:
    				break;
			
	        }
          } while(option != 2);
            
		}
}
