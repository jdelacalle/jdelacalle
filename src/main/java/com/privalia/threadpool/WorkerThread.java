package com.privalia.threadpool;
/*
 * Java thread pool manages the pool of
 * worker threads, it contains a queue that 
 */
public class WorkerThread implements Runnable {

	private String command;
	
	public WorkerThread(String s){
		this.command = s;
	}
	
	@Override
	public void run() {
		System.out.print(Thread.currentThread().getName() + "Start. Command = " + command); 
		processCommand();
		System.out.print(Thread.currentThread().getName() + "End.");
	}
	
	private void processCommand() {
		try{
			Thread.sleep(5000);
		} catch (InterruptedException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		return this.command;
	}
}
