package com.privalia.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

public class FileUtil {
	static final Logger logger = Logger.getLogger(FileUtil.class);
	private static File file = null;
	
	private FileUtil() {}
	public static synchronized boolean create(String name, String path) throws IOException
	{
		
		//synchronized es para el multithread en un metodo.
		boolean isFileCreated = false;

		StringBuilder builder = new StringBuilder();
		builder.append(name);
		builder.append(path);
		file = new File(builder.toString());
		if (file.exists()) {
			logger.warn("el ficheroi noi existe");
		} else{
			isFileCreated = file.createNewFile();
		}
		
		return isFileCreated;
	}
	
	@MethodInfo(author="Jorge de la calle",
			revision=1,
			comments="Writes line to a file",
			date = "03/09/2017")
	public static void write(IWritable item, File file) throws IOException
	{
		StringBuilder builder = new StringBuilder();
		builder.append(item.toString());
		builder.append(System.getProperty("line.separator"));
		try(FileWriter fwriter = new FileWriter(file, true)) {
			fwriter.write(builder.toString());	
		}
		
	}
	
	public static File getFile(){
		return file;
	}
	}
