package com.privalia.presentation.Integration.test;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.Test;

import com.privalia.dao.BaseDao;

public class BaseDaoIntegrationTest {

	@Test
	public void testGetConnection() throws SQLException {
		
		BaseDao bd=new BaseDao();
		assertTrue(bd.getConnection() != null);
	}
}
