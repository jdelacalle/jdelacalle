package com.privalia.presentation.Integration.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import com.privalia.presentation.Calculator;
import com.privalia.presentation.ICalculator;

public class CalculatorIntegrationTest {

	@Test
	public void testAdd()
	{	
		ICalculator calculator = new Calculator();
		assertTrue(calculator.add(2, 3) == 5);
	}
	@Test
	public void testSubstract()
	{
		ICalculator calculator = new Calculator();
		assertTrue(calculator.substract(2, 3) == 5);
	}
	@Test
	public void testMultiply()
	{
		ICalculator calculator = new Calculator();
		assertTrue(calculator.multiply(2, 3) == 5);
	}
	@Test
	public void testDivide()
	{
		ICalculator calculator = new Calculator();
		assertTrue(calculator.divide(2, 3) == 5);
	}
	
}
