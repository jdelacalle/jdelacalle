package com.privalia.presentation.Integration.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import com.privalia.dao.IDao;
import com.privalia.dao.INio;
import com.privalia.dao.StudentDao;
import com.privalia.model.Student;
import com.privalia.util.FileUtil;

public class StudentDaoTest {
	static final Logger logger = Logger.getLogger(StudentDaoTest.class);
	static Properties prop = null;
	static FileInputStream input = null;
	private static int idStudent = 0;
	
	public void testSetup(File f) {
		if (f.exists()) {
			f.delete();
		}
	}

	@Test
	public void testAdd() throws IOException {
		File StudentFile = new File("Students.txt");

		this.testSetup(StudentFile);

		Student studentTest = new Student(1, "Manuel", "Sierra Garcia fsdgradsg", 24);
		IDao studentDao = new StudentDao();

		studentDao.add(studentTest);

		Student alumnoEncontrado = findAlumno(studentTest.getIdStudent());
		/*
		 * BufferedReader bfr = new BufferedReader(new FileReader(StudentFile));
		 * String readStudent = bfr.readLine(); bfr.close();
		 */
		// recuoperas y lo comparas el leido con el de memoria creado.
		assertEquals(alumnoEncontrado, studentTest);
	}

	@BeforeClass
	public static void setup() {
		prop = new Properties();
		try {
			input = new FileInputStream("src/main/resources/config.properties");
			prop.load(input);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new ExceptionInInitializerError(e);// wrapping de exceptions
														// lanzando una runtime
														// excepcion ya que el
														// inicalizador estatico
														// no me deja lanzar
														// checked exceptions
		}
	}

	private Student findAlumno(int idAlumno) {
		boolean encontrado = false;
		String[] alumnoString = null;
		File fichero = FileUtil.getFile();
		try (Scanner s = new Scanner(fichero)) {
			while (s.hasNextLine() || encontrado == false) {
				String linea = s.nextLine();
				alumnoString = linea.split(",");

				if (Integer.parseInt(alumnoString[0].trim()) == idAlumno) {
					encontrado = false;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		Student student = new Student();

		if (encontrado == true) {

			student.setIdStudent(Integer.parseInt(alumnoString[0].trim()));
			student.setName(alumnoString[1].trim());
			student.setSurname(alumnoString[2].trim());
			student.setAge(Integer.parseInt(alumnoString[3].trim()));
		} else {
			student = null;
		}

		return student;
	}

	@Test
	public void testAddWithNio() throws IOException {
		File StudentFile = new File("Students.txt");

		this.testSetup(StudentFile);

		Student studentTest = new Student(1, "Manuel", "Sierra Garcia fsdgradsg", 24);
		INio<Student> studentDao = new StudentDao();

		studentDao.addWithNio(studentTest);

		Student alumnoEncontrado = findStudentWithNio(studentTest.getIdStudent());
		/*
		 * BufferedReader bfr = new BufferedReader(new FileReader(StudentFile));
		 * String readStudent = bfr.readLine(); bfr.close();
		 */
		// recuoperas y lo comparas el leido con el de memoria creado.
		assertEquals(alumnoEncontrado, studentTest);
	}

	public Student findStudentWithNio(int idAlumno) throws IOException {

		Student student = null;
		File StudentFile = new File("Students.txt");
		try (RandomAccessFile aFile = new RandomAccessFile(StudentFile, "rw");

				FileChannel inChannel = aFile.getChannel()) {

			ByteBuffer buf = ByteBuffer.allocate(512);

			StringBuffer line = new StringBuffer();

			String[] studentString = null;

			boolean finished = false;

			while (inChannel.read(buf) > 0 && !finished) {

				buf.flip();

				for (int i = 0; i < buf.limit(); i++) {

					char ch = ((char) buf.get());

					if (ch == '\n') {

						String stringLine = line.toString();

						studentString = stringLine.split(",");

						if (Integer.parseInt(studentString[0]) == idAlumno) {

							student = Student.getStudent();

							student.setName(studentString[1]);

							student.setSurname(studentString[2]);

							student.setAge(Integer.parseInt(studentString[3].trim()));

							finished = true;

							break;

						}

						line = new StringBuffer();

					} else {

						line.append(ch);

					}

				}

				buf.clear();

			}

		} catch (IOException e) {

			logger.error(e.getMessage());

			throw e;

		}

		return student;

	}
		
		@Test
		public void test1Add() throws SQLException {
			Student std = new Student();
			IDao<Student> studentDao = new StudentDao();
			std.setName("pablo");
			std.setSurname("garcia");
			std.setAge(24);
			idStudent = studentDao.addStudentWithMySql(std);
			assertTrue(idStudent > 0);
		}
	
}
